<!doctype html>

@php
    use GeoIp2\Database\Reader;
    $reader = new Reader('GeoLite2-Country.mmdb');
    $ip = Request::getClientIp();
    if($ip != "::1"){
        $record = $reader->country($ip);
    }else{
        $record = (object) [
            'country' => (object)['isoCode' => 'TW']
        ];
    }
    if(!in_array($record->country->isoCode, ['SG', 'HK', 'TW', 'MY', 'JP'])){
        $record = (object) [
            'country' => (object)['isoCode' => 'TW']
        ];
    }
@endphp

@include(strtolower($record->country->isoCode).'.index')
